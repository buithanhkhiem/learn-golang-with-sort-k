package test

import (
	"sort/handle"
	"testing"
)

func BenchmarkSelectionsort(b *testing.B) {
	arrayInput := []int{96, 48, 86, 68, 57, 82, 63, 70, 37, 34, 83, 27, 19, 97, 9, 17}

	for n := 0; n < b.N; n++ {
		handle.Selectionsort(arrayInput)
	}
}

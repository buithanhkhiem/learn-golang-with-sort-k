package handle

// func Partion(a []int, l int, h int) int {
// 	p := a[h]
// 	var i int = l - 1
// 	for index := 1; index < h; index++ {
// 		if a[index] < p {
// 			i++
// 		}
// 	}
// 	retutn i
// }
// func Quicksort(a []int) []int {
// 	h := len(a) - 1
// 	l := 0
// 	pi := Partion()
// 	if l < h {
// 		Quicksort(a[:pi])
// 		Quicksort(a[pi+1:])
// 	}
// }

func Quicksort(a []int) []int {
	if len(a) < 2 {
		return a
	}

	left, right := 0, len(a)-1

	pivot := len(a) - 1

	a[pivot], a[right] = a[right], a[pivot]

	for i, _ := range a {
		if a[i] < a[right] {
			a[left], a[i] = a[i], a[left]
			left++
		}
	}

	a[left], a[right] = a[right], a[left]

	Quicksort(a[:left])
	Quicksort(a[left+1:])

	return a
}

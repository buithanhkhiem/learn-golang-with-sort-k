package handle

func Insertionsort(items []int) {
	var n = len(items)
	for i := 1; i < n; i++ {
		j := i
		for j > 0 {
			if items[j-1] > items[j] {
				items[j-1], items[j] = items[j], items[j-1]
			}
			j = j - 1
		}
	}
}

// func main() {
// 	x := []int{96, 48, 86, 68, 57, 82, 63, 70, 37, 34, 83, 27, 19, 97, 9, 17}

// 	insertionsort(x)
// 	fmt.Println(x)
// }
